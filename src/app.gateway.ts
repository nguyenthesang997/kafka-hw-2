// import {
//   SubscribeMessage,
//   WebSocketGateway,
//   WebSocketServer,
// } from '@nestjs/websockets';
// import { Server, Socket } from 'socket.io';

import { Injectable } from '@nestjs/common';
import { EventListener } from 'nestjs-io-client';
import { ProducerService } from './kafka/producer/producer.service';

@Injectable()
export class AppGateway {
  constructor(private readonly producerService: ProducerService) { }
  @EventListener('connect')
  open() {
    console.log('The connection is established.');
  }

  @EventListener('ping')
  ping() {
    console.log('A ping is received from the server.');
  }

  @EventListener('reconnect')
  unexpectedResponse() {
    console.log('successful reconnection');
  }

  @EventListener('stockPrice')
  async upgrade(data: any) {
    await this.producerService.produce(data);
  }
}
