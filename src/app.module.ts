import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IoClientModule } from 'nestjs-io-client';
import { AppController } from './app.controller';
import { AppGateway } from './app.gateway';
import { AppService } from './app.service';
import { KafkaModule } from './kafka/kafka.module';
import { TestConsumer } from './test.consumer';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'hw-2',
      entities: [],
      synchronize: true,
    }),
    KafkaModule,
    IoClientModule.forRoot({
      uri: 'ws://localhost:3001',
      options: {
        reconnectionDelayMax: 10000,
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService, TestConsumer, AppGateway],
})
export class AppModule { }
