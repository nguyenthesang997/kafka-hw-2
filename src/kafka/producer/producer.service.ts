import {
  Injectable,
  OnApplicationShutdown,
  OnModuleInit,
} from '@nestjs/common';
import * as CircularJSON from 'circular-json';
import { Kafka, Producer } from 'kafkajs';
@Injectable()
export class ProducerService implements OnModuleInit, OnApplicationShutdown {
  private readonly kafka = new Kafka({
    brokers: ['localhost:29092'],
  });

  private readonly producer: Producer = this.kafka.producer();

  async onModuleInit() {
    await this.producer.connect();
  }

  async produce(record) {
    await this.producer.send({
      topic: 'test',
      messages: [
        {
          value: CircularJSON.stringify(record),
        },
      ],
    });
  }

  async onApplicationShutdown() {
    await this.producer.disconnect();
  }
}
