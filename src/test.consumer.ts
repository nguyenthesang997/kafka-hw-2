import { OnModuleInit } from '@nestjs/common';
import { Injectable } from '@nestjs/common/decorators';
import { ConsumerService } from './kafka/consumer/consumer.service';
@Injectable()
export class TestConsumer implements OnModuleInit {
  constructor(private readonly consumerService: ConsumerService) { }
  async onModuleInit() {
    await this.consumerService.consume(
      {
        topic: 'test',
      },
      {
        eachMessage: async ({ topic, partition, message }) => {
          console.log({
            value: JSON.parse(message.value.toString()),
            topic: topic.toString(),
            partition: partition.toString(),
          });
        },
      },
    );
  }
}
